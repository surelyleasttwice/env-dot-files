#!/bin/bash
NOWDATE=$(date +%Y%m%d.%T)
SCRIPTDIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# clone-or-pull repo-location folder-destination
clone-or-pull() 
{
    if [ -d $2 ]; then
        echo "Plugin already installed, updating..."
        cd $2
        git pull
    else
        git clone $1 $2
    fi
}

if [[ $# -eq 0 || "$1" == "full" ]]; then
    PACKAGES="vim tmux zsh git"
    echo "[INSTALL] Packages: $PACKAGES"

    if command -v apt > /dev/null; then
        sudo apt install $PACKAGES
    elif command -v yum > /dev/null; then
        sudo yum install $PACKAGES
    elif command -v brew > /dev/null; then
        brew install $PACKAGES
    fi
fi

if [[ "$1" == "full"  ||  "$1" == "plugins" ]]; then
    if [ ! -d ~/.oh-my-zsh ]; then
        echo
        echo "[INSTALL] oh-my-zsh..."
        curl -kL https://github.com/robbyrussell/oh-my-zsh/raw/master/tools/install.sh | sh
    fi

    echo
    echo "[INSTALL] zsh-syntax-highlighting..."
    mkdir -p ~/.oh-my-zsh/custom/plugins
    clone-or-pull git://github.com/zsh-users/zsh-syntax-highlighting.git ~/.oh-my-zsh/custom/plugins/zsh-syntax-highlighting

    echo
    echo "Installing Vundle..."
    mkdir -p ~/.vim/bundle
    clone-or-pull https://github.com/gmarik/vundle.git ~/.vim/bundle/vundle

    echo
    echo "Vundling vim plugins..."
    vim +BundleInstall! +qall
fi

for fullfile in $SCRIPTDIR/assets/dotfiles/*
do
    # Obtain the filename.ext
    f=$(basename "$fullfile")
    echo $f
    
    if [[ -e $HOME/.$f ||  -h $HOME/.$f ]]; then
        echo "[EXISTS] $HOME/.$f"
        echo "[COPY] $HOME/.$f --> $HOME/.rc_backup/.$f.$NOWDATE"
        mkdir -p $HOME/.rc_backup
        cp $HOME/.$f $HOME/.rc_backup/.$f.$NOWDATE
        unlink $HOME/.$f
    fi
    echo "[LINK] $HOME/.$f --> $SCRIPTDIR/assets/dotfiles/$f"
    ln -s $SCRIPTDIR/assets/dotfiles/$f $HOME/.$f
done
