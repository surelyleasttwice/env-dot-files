#!/bin/bash
dir=$1

if [[ -e "$dir/fb" ]]; then
    rm -rf "$dir/fb"
fi

mkdir -p "$dir/fb"

#find "$dir" -iname "*.jpg" -type f -exec sh -c 'export filename=$(basename "{}"); export loc=$(dirname "{}"); echo $filename; echo $loc; convert "{}" -resize 2048x2048\> -unsharp 0x3+0.7+0.0196 -quality 85% "$loc/fb/$filename"' \;
#find "$dir" \( -type d ! -wholename "$dir" -prune \) -o \( -iname "*.jpg" -type f \)  -exec sh -c 'export filename=$(basename "{}"); export loc=$(dirname "{}"); echo $filename; echo $loc;' \;
find "$dir" \( -type d ! -wholename "$dir" -prune \) -o \( -iname "*.jpg" -type f \)  -exec sh -c 'export filename=$(basename "{}"); export loc=$(dirname "{}"); echo $filename; convert "{}" -resize 2048x2048\> -unsharp 0x0.7 -quality 85% "$loc/fb/$filename"' \;