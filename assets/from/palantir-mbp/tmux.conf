# ========
# General
# ========

set -g prefix `
bind ` send-prefix
unbind C-b

set -s escape-time 0

set -g base-index 1
set -g buffer-limit 10
set -g default-terminal "screen-256color"
set -g history-limit 10000
set -g status-interval 10
set -g status-key vi
set -g default-shell /bin/zsh
set -g set-titles on
set -g set-titles-string '[#S:#I #H] #W'
set -g utf8 on

setw -g aggressive-resize on
setw -g mode-keys vi
setw -g automatic-rename
setw -g monitor-activity on

# =====
# Look
# =====

set -g status-bg default
set -g status-left-length 40
set -g status-left '#[fg=cyan]#H'
set-window-option -g window-status-current-format '#[fg=white]#I:#[fg=white,bold]#W#[fg=dim]#F'
set-window-option -g window-status-format '#[fg=white]#I:#[fg=white,bold]#W#[fg=dim]#F'
set -g status-right '#[fg=cyan]%Y-%m-%d %H:%M'

# Matthias' Colors
#set -g message-bg yellow
#set -g message-fg black
#
#set -g pane-active-border-bg default
#set -g pane-active-border-fg red
#set -g pane-border-bg default
#set -g pane-border-fg yellow
#
#set -g status-bg black
#set -g status-fg yellow
#set -g status-justify centre
#set -g status-left '%H:%M #[fg=red]| #[default]#H #[fg=red]| #[default]#I-#P '
#set -g status-left-length 50
#set -g status-right "#(.tmux-stats.sh)"
#set -g status-right-length 120
#
#set -g pane-active-border-bg default
#set -g pane-active-border-fg red
#set -g pane-border-bg default
#set -g pane-border-fg yellow
#
#setw -g clock-mode-colour yellow
#setw -g clock-mode-style 24
#setw -g window-status-current-fg red

# =============
# Key bindings
# =============

bind R source ~/.tmux.conf

#
# Window management
#

unbind Space
unbind n
unbind p
unbind y

bind w list-windows
bind . last-window

bind -r [ previous-window
bind -r ] next-window
bind -r < swap-window -t :-
bind -r > swap-window -t :+

bind -n F1 select-window -t 1
bind -n F2 select-window -t 2
bind -n F3 select-window -t 3
bind -n F4 select-window -t 4
bind -n F5 select-window -t 5
bind -n F6 select-window -t 6
bind -n F7 select-window -t 7
bind -n F8 select-window -t 8
bind -n F9 select-window -t 9
bind -n F10 select-window -t 10

#
# Pane management
#

bind e list-panes
bind \; last-pane

bind s split-window -v
bind v split-window -h

bind -r { swap-pane -U
bind -r } swap-pane -D
bind -r ( rotate-window -U
bind -r ) rotate-window -D

bind h select-pane -L
bind j select-pane -D
bind k select-pane -U
bind l select-pane -R
bind -r C-h resize-pane -L 1
bind -r C-j resize-pane -D 1
bind -r C-k resize-pane -U 1
bind -r C-l resize-pane -R 1

bind -r \ next-layout
bind -r | previous-layout

#
# Copy & paste
#

bind Space copy-mode
bind p paste-buffer
bind P paste-buffer -d
bind - delete-buffer
bind = list-buffers
bind + choose-buffer
